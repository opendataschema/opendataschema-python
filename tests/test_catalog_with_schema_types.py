from pathlib import Path

import pytest
import requests

from opendataschema import GitRef, GitSchemaReference, SchemaCatalog, URLSchemaReference

script_dir = Path(__file__).resolve().parent

class MockSession(requests.Session):

    class MockResponse:

        class Headers:

            def items(self):
                return []

        def __init__(self):
            self.text = '{}'
            self.headers = MockSession.MockResponse.Headers()
            self.status_code = 200

        def raise_for_status(self):
            pass

        def json(self):
            return {}

    def get(self, url, **kwargs):
        return MockSession.MockResponse()


@pytest.fixture()
def schema_catalog():
    catalog_path = script_dir / "fixtures" / "catalog_with_schema_types.json"
    return SchemaCatalog(source=catalog_path, session=MockSession())


def test_schemas_references(schema_catalog):
    assert schema_catalog.reference_by_name
    assert len(schema_catalog.reference_by_name) == 3

    adresses = schema_catalog.reference_by_name.pop('adresses')
    assert isinstance(adresses, GitSchemaReference)
    assert adresses.get_schema_type() == "tableschema"

    catalogue = schema_catalog.reference_by_name.pop('catalogue')
    assert isinstance(catalogue, GitSchemaReference)

    subventions = schema_catalog.reference_by_name.pop('subventions')
    assert isinstance(subventions, URLSchemaReference)


def test_schemas_references_schema_url(schema_catalog):
    adresses = schema_catalog.reference_by_name['adresses']
    master_url = "https://git.opendatafrance.net/scdl/adresses/raw/master/schema.json"
    assert adresses.get_schema_url() == master_url
    assert adresses.get_schema_url(ref=GitRef(name="master")) == master_url
    assert adresses.get_schema_url(ref=GitRef(name="foo")) == \
        "https://git.opendatafrance.net/scdl/adresses/raw/foo/schema.json"


def test_schemas_references_to_json(schema_catalog):
    adresses = schema_catalog.reference_by_name['adresses']
    adresses_json = adresses.to_json()
    assert adresses_json['name'] == 'adresses'
    assert adresses_json['git_type'] == 'gitlab'
    assert adresses_json['schema_type'] == 'tableschema'

    subventions = schema_catalog.reference_by_name['subventions']
    subventions_json = subventions.to_json()
    assert subventions_json['name'] == 'subventions'
    assert subventions_json['schema_type'] == 'jsonschema'


    # prenoms = schema_catalog.reference_by_name['prenoms']
    # prenoms_json = prenoms.to_json()
    # assert prenoms_json['name'] == 'prenoms'
    # assert prenoms_json['git_type'] == 'github'
