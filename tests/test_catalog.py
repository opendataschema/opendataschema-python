from pathlib import Path

import pytest

from opendataschema import GitRef, GitSchemaReference, SchemaCatalog, URLSchemaReference

script_dir = Path(__file__).resolve().parent


@pytest.fixture()
def schema_catalog():
    catalog_path = script_dir / "fixtures" / "catalog.json"
    return SchemaCatalog(source=catalog_path)


def test_catalog_from_content():
    catalog_content = {
        "version": 1,
        "schemas": [
            {"name": "a", "schema_url": "http://a.com/schema.json"}
        ]
    }
    schema_catalog = SchemaCatalog(source=catalog_content)
    assert schema_catalog is not None


def test_catalog_from_content_wrong_version():
    catalog_content = {
        "version": 2,
        "schemas": [
            {"name": "a", "schema_url": "http://a.com/schema.json"}
        ]
    }
    with pytest.raises(NotImplementedError):
        SchemaCatalog(source=catalog_content)


def test_schemas_descriptor(schema_catalog):
    assert schema_catalog.descriptor
    first_schema = schema_catalog.descriptor["schemas"][0]
    assert first_schema["name"] == "adresses"
    assert first_schema["doc_url"] == "https://scdl.opendatafrance.net/docs/schemas/adresses.html"
    assert first_schema["repo_url"] == "https://git.opendatafrance.net/scdl/adresses/"


def test_schemas_references(schema_catalog):
    assert schema_catalog.reference_by_name
    assert len(schema_catalog.reference_by_name) == 4

    adresses = schema_catalog.reference_by_name.pop('adresses')
    assert isinstance(adresses, GitSchemaReference)

    catalogue = schema_catalog.reference_by_name.pop('catalogue')
    assert isinstance(catalogue, GitSchemaReference)

    prenoms = schema_catalog.reference_by_name.pop('prenoms')
    assert isinstance(prenoms, GitSchemaReference)

    subventions = schema_catalog.reference_by_name.pop('subventions')
    assert isinstance(subventions, URLSchemaReference)


def test_schemas_references_schema_url(schema_catalog):
    adresses = schema_catalog.reference_by_name['adresses']
    master_url = "https://git.opendatafrance.net/scdl/adresses/raw/master/schema.json"
    assert adresses.get_schema_url() == master_url
    assert adresses.get_schema_url(ref=GitRef(name="master")) == master_url
    assert adresses.get_schema_url(ref=GitRef(name="foo")) == \
        "https://git.opendatafrance.net/scdl/adresses/raw/foo/schema.json"


def test_schemas_references_to_json(schema_catalog):
    adresses = schema_catalog.reference_by_name['adresses']
    adresses_json = adresses.to_json()
    assert adresses_json['name'] == 'adresses'
    assert adresses_json['git_type'] == 'gitlab'

    prenoms = schema_catalog.reference_by_name['prenoms']
    prenoms_json = prenoms.to_json()
    assert prenoms_json['name'] == 'prenoms'
    assert prenoms_json['git_type'] == 'github'
